# Proyecto para visualizar en kioskos fichas de personas desaparecidas

## Descripción

Encontré la necesidad de poder mostrar en un kiosko de anuncios las fichas de desaparecidas de una región de méxico.

## Contexto

No hay un repositorio actualizado, con OCR de las fichas de desapaeridos en formato legible para pantallas.


## Scripts

- [ ] Descarga de fichas
- [ ] Filtrado de las fichas
- [ ] OCR de las fichas
- [ ] Renombrar archivos con OCR obtenido
- [ ] Subir fichas en subcarpetas a un repositorio


```
cd existing_repo
git remote add origin https://0xacab.org/carlosm2/desaparecidesmx.git
git branch -M no-masters
git push -uf origin no-masters
```

## Descargar imagenes de sitios

- [ ] Ciudad de México
- [ ] Estado de México

## 

- [ ] Descargar datos del año
- [ ] Filtrar datos del mes
- [ ] Identificar "localizado"
- [ ] Quitar imagenes de localizados
- [ ] Interfase web kiosko tematica por estado, fechas
- [ ] Subir imagenes a sitio web kiosko


## Uso


## Roadmap

## Contributing
State if you are open to contributions and what your requirements are for accepting them.


## Authors and acknowledgment
@cacu 

## License
Licencia de Producción de Pares 

## Project status
on progress
